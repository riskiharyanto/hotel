var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var BookingSchema = new Schema({
	idbooking: String,
	nokamar: String,
	checkin: Date,
	checkout: Date,
	tamu: {
		tipeId: String,
		nomorId: String, 
		nama: String,
		alamat: String, 
		telp: Number
	}
});

mongoose.model('Booking', BookingSchema);