var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var TamuSchema = new Schema({
	tipeId: String,
	nomorId: String, 
	nama: String,
	alamat: String, 
	telp: Number
});

mongoose.model('Tamu', TamuSchema);