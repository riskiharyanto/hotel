var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var KamarSchema = new Schema({
	nokamar: Number,
	kelas: String,
	harga: Number,
	booked: [{}]
});

mongoose.model('Kamar', KamarSchema);