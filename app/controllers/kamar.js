var express = require('express'),
	router = express.Router(),
	mongoose = require('mongoose'),
	Kamar = mongoose.model('Kamar');

module.exports = function (app) {
  app.use('/', router); /*bisa jalan tapa v1*/
};


/*get kamars*/
router.get('/kamar', function (req, res, next) {
	var data = Kamar.find({});
	data.exec(function(err, result){
	 	if (err) return next(err);
	 	res.render('kamar',{
	 		datakamar: result
	 	});
	});	
});

/*router.get('kamar/')*/

router.get('/kamar/:id', function(req, res, next){
	var data = Kamar.find({});
	data.exec(function(err, result){
		if (err) return next(err);
		var data2 = Kamar.findOne({_id:req.params.id});
		data2.exec(function(err, result2){
			res.render('kamar',{
				datakamar: result,
				selectkamar: result2
			});
		});
	});
});


router.get('/kamar/delete/:id', function(req, res, next){
	var data = Kamar.remove({"_id": req.params.id});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		}else{
			console.log("berhasil dihapus");
			res.redirect('/kamar');
		};
	});
});


router.post('/kamar', function(req, res, next){
	if (req.body.nokamar == ""){

	} else {
		var data = Kamar.findOne({nokamar:req.body.nokamar});
		data.exec(function(err,result){
			if(result == null){
				var inputdata = new Kamar({	nokamar: req.body.nokamar, 
											kelas: req.body.kelas, 
											harga: req.body.harga, 
											booked: req.body.booked
										});
				inputdata.save(function(err){
					if(err){
						console.log(err);
					}else{
						console.log("input room done");
						res.redirect('/kamar');
					}
				})
			} else {
				var datanew = {nokamar: req.body.nokamar,
								kelas: req.body.kelas,
								harga: req.body.harga
								};
				var options = {};
				Kamar.update({"_id": req.body.id}, datanew, options, callback);
					function callback(err, numAffected){
						res.redirect('/kamar')
					};
			}
		})
	}	
});



router.put('/kamar/:id', function(req, res, next){
	var datanew = {nokamar: req.body.nokamar,
					kelas: req.body.kelas, 
					harga: req.body.harga
					};
	var options = {};
	Kamar.update({"_id": req.body.id}, datanew, options, callback);
		function callback(err, numAffected, result){
			res.json({result})
		};
});


/*delete kamar*/
router.delete('/kamar/:id', function(req,res,next){
	var data = Kamar.remove({"_id": req.params.id});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		}else{
			console.log("berhasil dihapus");
			res.json({
				message: "data kamar berhasil dihapus"
			});
		};
	});
});


/*get kamar yg belum dibooking */
router.get('/kamar/:in/:out', function(req,res,next){	
	var data = Kamar.find({'booked':{ $not: { $elemMatch:{$or:[{'in': {$gte: req.params.in, $lte: req.params.out }},
																{'out': {$gte: req.params.in, $lte: req.params.out }}]}}}
						 });
	data.select(['nokamar','kelas','harga']);
	data.exec(function(err,result){
		if(err) return handleError(err);
		if (result == null){
			res.status(404).json({
				error:'kamar full'
			})
		} else {
			/*res.json({result})*/
			res.render('searchkamar',{
				datakamar: result
			});
		}
	});
});

/*input booking kamar (web)*/
router.get('/kamar/booked/:nomor', function(req,res,next){
	var data = Kamar.findOne({nokamar: req.params.nomor});
	data.exec(function(err,result){
	var value={};
		if(result == null){
			res.json({
				error:'kamar not found'
			});
		} else {
			result.booked.forEach(function(val){
				if ((val.in>=req.body.checkin & val.in<=req.body.checkout)||(val.out>=req.body.checkin & val.out<=req.body.checkout)){
					value.check = 1;
					return value;
				}else {
					value.check = 0;
					return value;
				}
			})
			if (value.check == 0){
				var datanew = {$push:{booked:$and[{in:req.body.checkin},{out:req.body.checkout}]}};
				var options = {};
				Kamar.update({"_id":req.params.id},datanew,options,callback);
				function callback(err,numAffected){
					res.json({
						message:"booking kamar done"
					});
				};
			} else {
				res.status(409).json({message:"kamar sudah dibooking"})
			}
		}
	});	
});


/*input booking kamar (postman)*/
router.post('/kamar/booked/:id', function(req,res,next){
	var data = Kamar.findById(req.params.id);
	data.exec(function(err,result){
	var value={};
		if(result == null){
			res.status(404).json({
				error:'kamar not found'
			});
		} else {
			result.booked.forEach(function(val){
				if ((val.in>=req.body.booked.in & val.in<=req.body.booked.out)||(val.out>=req.body.booked.in & val.out<=req.body.booked.out)){
					value.check = 1;
					return value;
				}else {
					value.check = 0;
					return value;
				}
			})
			if (value.check == 0){
				var datanew = {$push:{booked: req.body.booked}};
				var options = {};
				Kamar.update({"_id":req.params.id},datanew,options,callback);
				function callback(err,numAffected){
					res.json({
						message:"booking kamar done"
					});
				};
			} else {
				res.status(409).json({message:"kamar sudah dibooking"})
			}
		}
	});	
});


/*delete booking kamar*/
router.delete('/kamar/booked/:id', function(req, res, next){
	var data = Kamar.findById(req.params.id);
	data.exec(function(err,result){
	var value={};
		if(result==null){
			res.status(404).json({
				error:'kamar not found'
			})
		}else{
			result.booked.forEach(function(val){
				if (val.in == req.body.booked.in & val.out == req.body.booked.out){
					value.check = 1;
					return value
				}else{
					value.check = 0;
					return value;
				}
			});
			if (value.check == 1){
				var datanew = {$pull:{booked:{$and:[{in: req.body.booked.in},{out: req.body.booked.out}]}}};
				var options = {};
				Kamar.update({"_id":req.params.id},datanew,options,callback);
				function callback(err, numAffected){
					res.json({
						message:"check out berhasil"
					});
				};
			}else{
				res.status(409).json({message:"check out fail"});
			}
		}
	});
});




