var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Kamar = mongoose.model('Kamar'),
  Booking = mongoose.model('Booking');

module.exports = function (app) {
  app.use('/', router);
};


router.get('/booking', function (req, res, next) {
	 res.render('searchkamar');
});


router.get('/booking/:id', function(req, res, next){
	var data = Kamar.findById(req.params.id);
	data.exec(function(err, result){
		if(err){

		}else{
			res.render('inputbookingkamar',{
				data: result
			});
		}
	});
});

router.post('/booking', function(req, res, next){
	var data = Booking.findOne({tamu:{$and:[{tipeId:req.body.tipeId},{nomorId:req.body.nomorId}]}});
	data.exec(function(err, result){
		if(result == null){
			var inputdata = new Booking({ 	idbooking: req.body.idbooking,
											nokamar: req.body.nokamar,
											checkin: req.body.checkin,
											checkout: req.body.checkout,
											tamu:{
												tipeId: req.body.tipeId,
												nomorId: req.body.nomorId,
												nama: req.body.nama,
												alamat: req.body.alamat,
												telp: req.body.telp
											}
										});
			inputdata.save(function(err){
				if(err){
					console.log(err);
				}else{
					var data = Kamar.findById(req.body.id);
					data.exec(function(err,result){
					var value={};
						if(result == null){
							res.json({
								error:'kamar not found'
							});
						} else {
							result.booked.forEach(function(val){
								if ((val.in>=req.body.checkin & val.in<=req.body.checkout)||(val.out>=req.body.checkin & val.out<=req.body.checkout)){
									value.check = 1;
									return value;
								}else {
									value.check = 0;
									return value;
								}
							})
							if (value.check == 0){
								/*var datanew = {$push:{booked:{in:req.body.checkin},{out:req.body.checkout}}};
								var options = {};
								Kamar.update({"_id":req.params.id},datanew,options,callback);
								function callback(err,numAffected){
									res.json({
										message:"booking kamar done"
									});
								};*/
							} else {
								res.status(409).json({message:"kamar sudah dibooking"})
							}
						}
					});	
				}
			});
		}else{
			res.json({
				notif:"tamu can't booking"
			})
		}
	});
})
